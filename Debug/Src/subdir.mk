################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Src/ax25.c \
../Src/ax5043.c \
../Src/bsp_driver_sd.c \
../Src/fatfs.c \
../Src/fatfs_platform.c \
../Src/freertos.c \
../Src/main.c \
../Src/pq9ish.c \
../Src/sd_diskio.c \
../Src/spi_utils.c \
../Src/stm32l4xx_hal_msp.c \
../Src/stm32l4xx_hal_timebase_tim.c \
../Src/stm32l4xx_it.c \
../Src/syscalls.c \
../Src/system_stm32l4xx.c 

OBJS += \
./Src/ax25.o \
./Src/ax5043.o \
./Src/bsp_driver_sd.o \
./Src/fatfs.o \
./Src/fatfs_platform.o \
./Src/freertos.o \
./Src/main.o \
./Src/pq9ish.o \
./Src/sd_diskio.o \
./Src/spi_utils.o \
./Src/stm32l4xx_hal_msp.o \
./Src/stm32l4xx_hal_timebase_tim.o \
./Src/stm32l4xx_it.o \
./Src/syscalls.o \
./Src/system_stm32l4xx.o 

C_DEPS += \
./Src/ax25.d \
./Src/ax5043.d \
./Src/bsp_driver_sd.d \
./Src/fatfs.d \
./Src/fatfs_platform.d \
./Src/freertos.d \
./Src/main.d \
./Src/pq9ish.d \
./Src/sd_diskio.d \
./Src/spi_utils.d \
./Src/stm32l4xx_hal_msp.d \
./Src/stm32l4xx_hal_timebase_tim.d \
./Src/stm32l4xx_it.d \
./Src/syscalls.d \
./Src/system_stm32l4xx.d 


# Each subdirectory must supply rules for building sources it contributes
Src/%.o: ../Src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32L476xx -I"/home/george/Desktop/pq9ish-0.9.2_Test/Inc" -I"/home/george/Desktop/pq9ish-0.9.2_Test/Drivers/STM32L4xx_HAL_Driver/Inc" -I"/home/george/Desktop/pq9ish-0.9.2_Test/Drivers/STM32L4xx_HAL_Driver/Inc/Legacy" -I"/home/george/Desktop/pq9ish-0.9.2_Test/Middlewares/Third_Party/FatFs/src" -I"/home/george/Desktop/pq9ish-0.9.2_Test/Middlewares/Third_Party/FreeRTOS/Source/include" -I"/home/george/Desktop/pq9ish-0.9.2_Test/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS" -I"/home/george/Desktop/pq9ish-0.9.2_Test/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F" -I"/home/george/Desktop/pq9ish-0.9.2_Test/Drivers/CMSIS/Device/ST/STM32L4xx/Include" -I"/home/george/Desktop/pq9ish-0.9.2_Test/Drivers/CMSIS/Include"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


