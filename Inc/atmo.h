/*
 *  A PQ9 based Weather Station
 *
 *  Copyright (C) 2017 Libre Space Foundation
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ATMO_H_
#define ATMO_H_

#define PQ_ATMO_NODE_BASEID 0x04
#define PQ_ATMO_NODE_EXTID 0x0
#define PQ_ATMO_MESSAGE_MASK 0x3F

typedef enum {
  PQ_ATMO_PING = 0,
  PQ_ATMO_MUON_POWER,
  PQ_ATMO_PYRO_POWER,
  PQ_ATMO_PREHEAT_MICS,
  PQ_ATMO_MUON_GET_FREQUENCY
} atmo_commands_t;

typedef enum {
  PQ_ATMO_ATMOSPHERIC_DATA=0,
  PQ_ATMO_MUON,
  PQ_ATMO_MICS,
  PQ_STATE,
  PQ_SENSOR2,
  PQ_ATMO_INIT_REQUEST,
  PQ_ATMO_INIT_DATA
} CAN_message_id;

typedef enum {
        ASCENDING = 0,
        DESCENDING,
        FLOATING,
        FLIGHT_STATE_INVALID
} flight_phase_t;

typedef enum {
        CAMERA_POWER = 0,
        PYRO_POWER = 1,
        MICS_POWER = 2,
        BUZZER_POWER = 3
} system_state_t;

typedef struct {
        flight_phase_t flightPhase;
        int16_t verticalVelocity;
        uint8_t state_control_timer_speed;
        uint8_t system_state;
        uint16_t pad1;
} state_data_t;

typedef enum {
  GROUND = 0,
  FLYING,
  LANDING,
  LANDED
} mission_phase_t;
typedef struct {
  uint16_t value1;
  uint16_t value2;
  uint16_t value3;
  uint16_t value4;
  CAN_message_id  message_type;
} pq_comms_message_t;

typedef struct {
        uint16_t pyro_alt;
        uint16_t highestLandPoint;
        mission_phase_t mission_phase;
} initialize_data_t;
#endif /* ATMO_H_ */
